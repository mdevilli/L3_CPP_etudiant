#include "liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, Liste_elementInexistant)  {
    Liste l;
    CHECK_EQUAL(l.getElement(5),-1);
}

TEST(GroupListe, Liste_vide)  {
    Liste m;
    CHECK_EQUAL(m.getTaille(),0);
}

TEST(GroupListe, Liste_unElem)  {
    Liste m;
    m.ajouterDevant(5);
    CHECK_EQUAL(m.getElement(1),5);
}
