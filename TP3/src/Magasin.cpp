#include "Magasin.hpp"

Magasin::Magasin() {
    _idCourantClient = 0;
    _idCourantProduit = 0;
}

int Magasin::nbClients() const {
    return _clients.size();
}

void Magasin::ajouterClient(const std::string &nom){
    Client c(_idCourantClient, nom);
    _clients.push_back(c);
    _idCourantClient++;
}

void Magasin::afficherClients() const{
    for(int i=0;i<nbClients();i++){
        _clients[i].afficherClient();
    }
}

void Magasin::supprimerClient(int idClient){
    for(int i=0;i<nbClients();i++){
        if(_clients[i].getId()==idClient)
            {
            std::swap(_clients[i],_clients[nbClients()-1]);
            _clients.pop_back();
            return;
        }
    }
    throw std::string("Erreur: ce client n'existe pas");
}

int Magasin::nbProduits() const {
    return _produits.size();
}

void Magasin::ajouterProduit(const std::string &nom){
    Produit p(_idCourantProduit, nom);
    _produits.push_back(p);
    _idCourantProduit++;
}

void Magasin::afficherProduits() const{
    for(int i=0;i<nbProduits();i++){
        _produits[i].afficherProduit();
    }
}

void Magasin::supprimerProduit(int idProduit){
    for(int i=0;i<nbProduits();i++){
        if(_produits[i].getId()==idProduit)
            {
            std::swap(_produits[i],_produits[nbProduits()-1]);
            _produits.pop_back();
            return;
        }
    }
    throw std::string("Erreur: ce produit n'existe pas");
}

int Magasin::nbLocations() const{
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit){
    Location l = { idClient, idProduit };
    _locations.push_back(l);
}

void Magasin::afficherLocations()const{
    for(int i=0;i<nbLocations();i++){
        _locations[i].afficherLocation();
    }
}

void Magasin::supprimerLocation(int idCLient, int idProduit){
    for(int i=0;i<nbLocations();i++){
        if(_locations[i]._idClient==idCLient && _locations[i]._idProduit==idProduit)
            {
            std::swap(_locations[i],_locations[nbLocations()-1]);
            _produits.pop_back();
            return;
        }
    }
    throw std::string("Erreur: cette location n'existe pas");
}

bool Magasin::trouverClientDansLocation(int idClient) const{
    for(int i=0; i<nbLocations(); i++){
        if(_locations[i]._idClient == idClient) return true;
    }
    return false;
}
std::vector<int> Magasin::calculerClientsLibres() const{
    std::vector<int> vec;
    for(int i=0;i<nbClients();i++){
        if(trouverClientDansLocation(i)==false){
            vec.push_back(i);
        }
    }
    return vec;
}

bool Magasin::trouverProduitDansLocation(int idProduit) const{
    for(int i=0; i<nbLocations(); i++){
        if(_locations[i]._idProduit == idProduit) return true;
    }
    return false;
}
std::vector<int> Magasin::calculerProduitsLibres() const{
    std::vector<int> vec;
    for(int i=0;i<nbProduits();i++){
        if(trouverProduitDansLocation(i)==false){
            vec.push_back(i);
        }
    }
    return vec;
}
