#include "Produit.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, Produit_test1)  {
    Produit p(6, "Le QI de Valentin");
    CHECK_EQUAL(p.getId(), 6);
}

TEST(GroupProduit, Produit_test2)  {
    Produit p(6,"Le QI de Valentin");
    CHECK_EQUAL(p.getDescription(), "Le QI de Valentin");
}
