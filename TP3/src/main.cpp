#include <iostream>
#include "location.hpp"
#include "Client.hpp"
#include "Produit.hpp"
#include "Magasin.hpp"

int main() {

    Magasin m;
    std::cout<<m.nbProduits()<<std::endl;
    m.ajouterProduit("Matthias");
    m.ajouterProduit("Valentin");
    m.ajouterProduit("Pierre");
    m.afficherProduits();
    try{
        m.supprimerProduit(9);
    }
    catch(std::string s){
        std::cerr << "exception " << s << std::endl;
    }

    m.supprimerProduit(0);
    m.afficherProduits();

    return 0;
}
