#ifndef MAGASIN_HPP
#define MAGASIN_HPP

#include "Client.hpp"
#include "Produit.hpp"
#include "location.hpp"

#include <vector>
#include <string>

class Magasin
{
    private:
        std::vector<Client> _clients;
        std::vector<Produit> _produits;
        std::vector<Location> _locations;
        int _idCourantClient;
        int _idCourantProduit;
    public:
        Magasin();
        int nbClients() const;
        void ajouterClient(const std::string &nom);
        void afficherClients()const;
        void supprimerClient(int idCLient);

        int nbProduits() const;
        void ajouterProduit(const std::string &nom);
        void afficherProduits()const;
        void supprimerProduit(int idProduit);

        int nbLocations() const;
        void ajouterLocation(int idClient, int idProduit);
        void afficherLocations()const;
        void supprimerLocation(int idCLient, int idProduit);
        bool trouverClientDansLocation(int idClient) const;
        std::vector<int> calculerClientsLibres() const;
        bool trouverProduitDansLocation(int idProduit) const;
        std::vector<int> calculerProduitsLibres() const;
};

#endif // MAGASIN_HPP
