#include "Livre.hpp"

Livre::Livre()
{
    _titre = "";
    _auteur = "";
}

Livre::Livre(const std::string & titre,
             const std::string & auteur,
             int annee) :
    _titre(titre),
    _auteur(auteur),
    _annee(annee)
{
    if(_titre.find("\n") != std::string::npos)
        throw  std::string("Erreur: titre invalide ('\n' non autorisé");
    if(_titre.find(";") != std::string::npos)
        throw  std::string("erreur : titre non valide (';' non autorisé)");
    if(_auteur.find("\n") != std::string::npos)
        throw  std::string("erreur : auteur non valide ('\n' non autorisé)");
    if(_auteur.find(";") != std::string::npos)
        throw  std::string("erreur : auteur non valide (';' non autorisé)");
}

const std::string & Livre::getTitre() const
{
    return this->_titre;
}

const std::string & Livre::getAuteur() const
{
    return this->_auteur;
}

int Livre::getAnnee() const
{
    return this->_annee;
}

bool Livre::operator<(const Livre & l2) const
{
    if( _auteur < l2._auteur) return true;
    else return _auteur == l2._auteur && _titre < l2._titre;
}

bool operator==(const Livre & l1, const Livre &l2)
{
    return l1.getTitre() == l2.getTitre() &&
           l1.getAuteur() == l2.getAuteur() &&
           l1.getAnnee() == l2.getAnnee();
}

void operator<<(std::ostream & os, const Livre & l)
{
    os << l.getTitre() << ";" << l.getAuteur() << ";" << l.getAnnee();
}

