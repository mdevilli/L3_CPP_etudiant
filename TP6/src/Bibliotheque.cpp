#include "Bibliotheque.hpp"
#include <iostream>
#include <algorithm>
#include <fstream>

Bibliotheque::Bibliotheque()
{

}

void Bibliotheque::afficher() const
{
    for(const Livre & l : *this)
    {
        std::cout << l;
    }
}

void Bibliotheque::trierParAuteurEtTitre()
{
    sort(begin(), end());
}

void Bibliotheque::trierParAnnee()
{
    auto cmp =[](const Livre & a, const Livre & b)
    {
        return a.getAnnee() < b.getAnnee();
    };
    sort(begin(), end(), cmp);
}

void Bibliotheque::ecrireFichier(const std::string & nomFichier) const
{
    std::ofstream ofs(nomFichier);
    for (const Livre & l : *this) ofs << l;
}
