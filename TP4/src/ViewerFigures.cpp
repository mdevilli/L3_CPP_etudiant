#include "ViewerFigures.hpp"

ViewerFigures::ViewerFigures(int argc, char ** argv)
{
    _kit = Gtk::Main(argc, argv);
    _window.set_title("Fenêtre Test");
    _window.set_default_size(640,480);
}

void ViewerFigures::run()
{
    _window.add(_dessin);
    _kit.run(_window);
}
