#include <iostream>
#include "point.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include "FigureGeometrique.hpp"
#include <gtkmm.h>
#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"

int main(int argc, char ** argv){
    ViewerFigures test(argc, argv);
    test.run();


}
